package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.content.ServiceConnection;


public class OptionMenu extends AppCompatActivity{

    private CheckBox sound;
    private CheckBox music;
    private String result;
    private String file_name = "Option.txt";

    public BGM mmService;

    boolean mBound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        result = FileReadWrite.readFile(this,file_name);
        setContentView(R.layout.activity_3);
        sound = findViewById(R.id.Sound);
        music = findViewById(R.id.Music);
        sound.setOnCheckedChangeListener(CheckListener);
        music.setOnCheckedChangeListener(CheckListener);
        if (result.startsWith("0"))
            sound.setChecked(false);
        else if (result.startsWith("1"))
            sound.setChecked(true);
        if (result.endsWith("0"))
            music.setChecked(false);
        else if (result.endsWith("1")) {
            music.setChecked(true);
        }
        Intent service = new Intent(getApplicationContext(), BGM.class);
        bindService(service, mServiceConnection, Service.BIND_AUTO_CREATE);

    }


    private CheckBox.OnCheckedChangeListener CheckListener = new CheckBox.OnCheckedChangeListener(){
        String sound_set, music_set;
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
            //   Toast.makeText(getApplicationContext(), "CheckListener", Toast.LENGTH_SHORT).show();

            //    Toast.makeText(getApplicationContext(), "bind", Toast.LENGTH_SHORT).show();

            if(sound.isChecked())
                sound_set = "1";
            else
                sound_set="0";
            if(music.isChecked()){
                music_set="1";
                if(mBound){
                    try{
                        MainMenu.mService.play();
                        Toast.makeText(getApplicationContext(), "music:1", Toast.LENGTH_SHORT).show();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
            else {
                music_set = "0";
                if(mBound) {
                    try {
                        MainMenu.mService.stop();
                        Toast.makeText(getApplicationContext(), "music:0", Toast.LENGTH_SHORT).show();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }

            String option_setting = sound_set + "/n" + music_set;
            FileReadWrite.writeFile(getApplicationContext(),file_name,option_setting);
        }
    };
    /*
    public void readFile(String FileName){
        FileInputStream file_op;
        try {
           // Toast.makeText(getApplicationContext(), "file found", Toast.LENGTH_SHORT).show();
            StringBuilder sb = new StringBuilder();
            file_op = this.openFileInput(FileName);
            byte[] data = new byte[file_op.available()];
            while(file_op.read(data)!=-1){
                sb.append(new String(data));
            }
            file_op.close();
            result = sb.toString();
           // Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void writeFile(String FileName, String FileContent) {
        FileOutputStream file_op;
            try{
                file_op = openFileOutput(FileName, Context.MODE_PRIVATE);
                file_op.write(FileContent.getBytes());
                file_op.close();
             //   Toast.makeText(getApplicationContext(), "Option.txt change", Toast.LENGTH_SHORT).show();
            }catch (IOException e) {
                e.printStackTrace();
              //  Toast.makeText(getApplicationContext(), "no change", Toast.LENGTH_SHORT).show();
            }
    }*/

    @Override
    protected void onResume(){
        Toast.makeText(getApplicationContext(),"OnResume1", Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    protected  void onPause(){
        if(mBound) {
            unbindService(mServiceConnection);
            Toast.makeText(getApplicationContext(), "OnPause1", Toast.LENGTH_SHORT).show();
            mBound = false;
        }
        super.onPause();
    }


    @Override
    protected void onStop(){
        if(mBound) {
            unbindService(mServiceConnection);
            Toast.makeText(getApplicationContext(), "OnStop1", Toast.LENGTH_SHORT).show();
            mBound = false;
        }
        super.onStop();
        //stopService(new Intent(this, BGM.class));
    }

    @Override
    protected void onDestroy(){
        //stopService(new Intent(this, BGM.class));
        if(mBound) {
            unbindService(mServiceConnection);
            Toast.makeText(getApplicationContext(), "OnDestroy1", Toast.LENGTH_SHORT).show();
            mBound = false;
        }
        super.onDestroy();
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BGM.LocalBinder serviceBinder = (BGM.LocalBinder) service;
            mmService = serviceBinder.getService();
            mBound = true;
            //     Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
            // if(!music.isChecked()){

            //Toast.makeText(getApplicationContext(), "stop", Toast.LENGTH_SHORT).show();
            // MainMenu.mService.stop();
            //}

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mmService = null;
            mBound=false;
        }
    };
}
