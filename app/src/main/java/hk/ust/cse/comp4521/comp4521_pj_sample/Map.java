package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.view.MotionEvent;

import java.util.ArrayList;

/**
 * Created by TKs on 19/5/2018.
 */

public class Map {

    private Bitmap image;
    private Bitmap subimage;
    private int x,y,dx,speed;
    private int direction;  // up=0 right=1 down =2 left=3
    private Player player;
    private ArrayList<Bullet> bullets;
    private ArrayList<Bullet> enemybullets;
    private ArrayList<Enemy> enemies;
    private ArrayList<Enemy> deads;

    public static boolean gameover=false;


    private int score;
    private int finalScore;

    private int pitch,roll; //pitch roll are in magnitude, original/10 round to int
    private int mydegree;
    private boolean move;

    private int fireCooldown=0;
    private int numberOfEnemies;
    private int maxEnemiesOnMap;
    private int movedelay;

    public Map(Context context, Bitmap res){

        image=res;

        x=image.getHeight();
        y=image.getWidth();

        subimage = image.createBitmap(image, (x - Constants.SCREEN_WIDTH / 2)/2, (y - Constants.SCREEN_HEIGHT / 2)/2, Constants.SCREEN_WIDTH/2, Constants.SCREEN_HEIGHT/2);

        Constants.MAP_WIDTH=image.getWidth()*2;
        Constants.MAP_HEIGHT=image.getHeight()*2;

        player=new Player(x,y,50);
        bullets=new ArrayList<Bullet>();
        enemybullets=new ArrayList<Bullet>();
        enemies=new ArrayList<Enemy>();
        deads = new ArrayList<Enemy>();

        gameover=false;
        movedelay =0;
        score=0;
        numberOfEnemies=20;
        maxEnemiesOnMap=7;


        for(int i=0;i<maxEnemiesOnMap;i++)
        {
            int newX=x;
            int newY=y;
            boolean validSpawn=true;
            while(Math.abs(newX-x)<500&&Math.abs(newY-y)<500&&validSpawn) {
                newX = (int) (Math.random() * Constants.MAP_WIDTH);
                newY = (int) (Math.random() * Constants.MAP_HEIGHT);
                validSpawn=true;
                for(Enemy e:enemies){
                    if(e.checkNearbyFriend(newX,newY)){
                        validSpawn=false;
                    }
                }
            }
            enemies.add(new Enemy(newX,newY));
            numberOfEnemies--;
        }
    }

    public void update(){

        Constants.CenterX=x;
        Constants.CenterY=y;
        Constants.Left=x-Constants.SCREEN_WIDTH/2;
        Constants.Right=x+Constants.SCREEN_WIDTH/2;
        Constants.Top=y-Constants.SCREEN_HEIGHT/2;
        Constants.Bottom=y+Constants.SCREEN_HEIGHT/2;

        subimage = image.createBitmap(image, (x - Constants.SCREEN_WIDTH / 2)/2, (y - Constants.SCREEN_HEIGHT / 2)/2, Constants.SCREEN_WIDTH/2, Constants.SCREEN_HEIGHT/2);

        if((player.returnHealth()<=0||(enemies.size()==0&&numberOfEnemies==0))&&!gameover){
            if(player.returnHealth()<=0){if(MainGame.sound_set.equals("1")) player.playExplosionSound();}
            gameover=true;
            if(score!=-1){
                if(player.returnHealth()>0)
                    score+=player.returnHealth();
                finalScore=score+0;
                String old_score=FileReadWrite.readFile(Constants.CURRENT_CONTEXT, "Score.txt");
                if(old_score==""||old_score==null){old_score="0";}
                int oldscore=Integer.parseInt(old_score);
                if(oldscore<=score)
                    FileReadWrite.writeFile(Constants.CURRENT_CONTEXT, "Score.txt", score+"");
                score=-1;
            }
        }

        if(!gameover)
        {
            player.update(x, y);
            if (fireCooldown > 0)
                fireCooldown--;

            //update bullet status
            ArrayList<Bullet> newList = new ArrayList<Bullet>();
            for (Bullet b : bullets) {
                b.update();
                if (b.checkWithinMap() && b.returntimeToLive() > 0) {
                    newList.add(b);
                }
                for (Enemy e : enemies)
                    if (b.checkEnemyCollision(e.getX(), e.getY())) {
                        if(MainGame.sound_set.equals("1")) {
                            Constants.ENEMY_GET_HIT_SOUND.start();
                        }
                        e.setHealth(-1);
                        newList.remove(b);
                    }
            }
            bullets = newList;
            //finish update bullet status

            //update enemy status
            ArrayList<Enemy> alive=new ArrayList<Enemy>();
            for (Enemy e : enemies) {
                boolean move = true;

                for (Enemy friend : alive) {
                    if (e.checkNearbyFriend(friend.getX(), friend.getY())) {
                        move = false;
                        e.updateNotMoving(player.getX(), player.getY());
                    }
                }
                if (e.returnHealth() > 0) {
                    alive.add(e);
                    if (e.returnFireCooldown() == 0&&player.returnHealth()>0) {
                        enemybullets.add(e.fire());
                        if(MainGame.sound_set.equals("1")) {
                            Constants.ENEMY_SHOOT_SOUND.start();
                        }
                        e.setFireCooldown((int) (Math.random() * 30) + 30);
                    }
                }else{score+=10;deads.add(e);if(MainGame.sound_set.equals("1")){Constants.EXPLOSION_SOUND.start();}}
                if (move) {
                    e.update(player.getX(), player.getY());
                }

            }
            enemies =alive;
            //finish update enemy status

            //update enemy bullet status
            ArrayList<Bullet> newList2 = new ArrayList<Bullet>();
            for (Bullet b : enemybullets) {
                b.update();
                if (b.checkWithinMap() && b.returntimeToLive() > 0) {
                    newList2.add(b);
                }
                System.out.print(b.checkEnemyCollision(player.getX(), player.getY()));
                if (b.checkEnemyCollision(player.getX(), player.getY())) {
                    player.setHealth(-1);
                    if(MainGame.sound_set.equals("1")) {
                        Constants.PLAYER_GET_HIT_SOUND.start();
                    }
                    newList2.remove(b);
                }
            }
            enemybullets = newList2;
            //finish update enemy bullet status

            //add enemies
            if(numberOfEnemies>0)
                for(int i=enemies.size();i<maxEnemiesOnMap;i++)
                {
                    int newX=x;
                    int newY=y;
                    boolean validSpawn=true;
                    while(Math.abs(newX-x)<500&&Math.abs(newY-y)<500&&validSpawn) {
                        newX = (int) (Math.random() * Constants.MAP_WIDTH);
                        newY = (int) (Math.random() * Constants.MAP_HEIGHT);
                        validSpawn=true;
                        for(Enemy e:enemies){
                            if(e.checkNearbyFriend(newX,newY)){
                                validSpawn=false;
                            }
                        }
                    }
                    enemies.add(new Enemy(newX,newY));
                    numberOfEnemies--;
                }
            //finish add enemies
        }
    }
    public void draw(Canvas canvas){

        canvas.drawBitmap(subimage,null,new RectF(0,0,Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT),null);

        player.draw(canvas);

        for (Bullet b : bullets) {
            b.draw(canvas);
        }
        for (Bullet b : enemybullets) {
            b.draw(canvas);
        }
        for (Enemy e : enemies) {
            e.draw(canvas);
        }
        for (Enemy e : deads) {
            e.draw(canvas);
            if(!e.isAlive())deads.remove(e);
        }

        canvas.drawBitmap(Constants.HEALTHBARIMAGE.returnHealthBarImage(player.returnHealth(),player.returnTotalHealth()),null,new RectF(0,0,Constants.SCREEN_WIDTH*0.15f,Constants.SCREEN_HEIGHT*0.1f),null);
        Paint mpaint = new Paint();
        mpaint.setColor(Color.MAGENTA);
        mpaint.setTextSize(80);
        canvas.drawText(player.returnHealth()+" / " + player.returnTotalHealth(), Constants.SCREEN_WIDTH*0.15f,Constants.SCREEN_HEIGHT*0.1f , mpaint);
        canvas.drawText("Remaining Enemies: "+(numberOfEnemies+enemies.size()),Constants.SCREEN_WIDTH*0.01f,Constants.SCREEN_HEIGHT*0.97f , mpaint);
        /*Paint mpaint = new Paint();
        mpaint.setColor(Color.MAGENTA);
        mpaint.setTextSize(50);
        canvas.drawText("Remain Enemy " + enemies.size(), 20, 250, mpaint);
        canvas.drawText("Player's health " + player.returnHealth(), 20, 300, mpaint);
        canvas.drawText("Score " + score, 20, 350, mpaint);

            for (int j = 0; j < enemies.size(); j++) {
                canvas.drawText("Enemy " + j + " at (" + enemies.get(j).getX() + ", " + enemies.get(j).getY() + ")", 20, 150 + j * 50, mpaint);
            }*/

        if(gameover) {
            canvas.drawBitmap(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.gameover),null,new RectF(0,Constants.SCREEN_HEIGHT*0.25f,Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT*0.55f),null);
            ScoreImage scoreImage=new ScoreImage(finalScore);
            canvas.drawBitmap(scoreImage.returnScoreBoard(),null,new RectF(Constants.SCREEN_WIDTH*0.25f,Constants.SCREEN_HEIGHT*0.55f,Constants.SCREEN_WIDTH*0.75f,Constants.SCREEN_HEIGHT*0.65f),null);
        }
    }
    public void setVector(int dx){
        this.dx=dx;
    }
    public void setMoveSpeed(int vec){
        this.speed=vec;
    }

    public void receiveTouch(MotionEvent event){

        float X=event.getX();
        float Y=event.getY();

        int choice=0;


        if(!gameover) {
            float angle = (float) Math.toDegrees(Math.atan2(Y-Constants.SCREEN_HEIGHT/2,X-Constants.SCREEN_WIDTH/2))+90;
            if(angle < 0){
                angle += 360;
            }
            player.updateDirection((int)angle,1);
            if (fireCooldown == 0) {
                bullets.add(player.fire());
                if(MainGame.sound_set.equals("1")) {
                    // Toast.makeText(Constants.CURRENT_CONTEXT, MainGame.sound_set, Toast.LENGTH_SHORT).show();
                    Constants.PLAYER_SHOOT_SOUND.start();
                }
                fireCooldown = 5;
            }
        }
    }

    public void updateMap(double pitch, double roll){
        this.pitch=(int)Math.round(pitch/10);
        this.roll=(int)Math.round(roll/10);
        if((Math.abs(this.pitch)>9&&Math.abs(this.roll)>9)||(this.pitch==0&&this.roll==0)){move=false;mydegree=0;}
        else{
            if(movedelay ==0) {
                float angle = (float) Math.toDegrees(Math.atan2(this.roll, this.pitch)) - 90;
                if (angle < 0) {
                    angle += 360;
                }
                move = true;
                mydegree = (int) Math.round(angle);
                player.updateDirection((int)mydegree,0);

                int dy = Constants.TABLEOFDXDY[mydegree / 5][1] / 4;
                int dx = Constants.TABLEOFDXDY[mydegree / 5][0] / 4;
                for(Enemy e:enemies){
                    if(Math.abs(e.getX()-(x+dx))<200&&Math.abs(e.getY()-(y+dy))<200)
                    {
                        move=false;
                        break;
                    }
                }
                if(move){
                y+=dy;
                x+=dx;
                if (x - Constants.SCREEN_WIDTH / 2 < 0) {
                    x = Constants.SCREEN_WIDTH / 2;
                }
                if (x + Constants.SCREEN_WIDTH / 2 > Constants.MAP_WIDTH) {
                    x = Constants.MAP_WIDTH - Constants.SCREEN_WIDTH / 2;
                }
                if (y - Constants.SCREEN_HEIGHT / 2 < 0) {
                    y = Constants.SCREEN_HEIGHT / 2;
                }
                if (y + Constants.SCREEN_HEIGHT / 2 > Constants.MAP_HEIGHT) {
                    y = Constants.MAP_HEIGHT - Constants.SCREEN_HEIGHT / 2;
                }
                movedelay=5;
                }
            }else{movedelay--;}
        }

    }

    /*public void stopexplosion_sound(){
        player.explosion_sound.release();
        player.explosion_sound = null;
        Toast.makeText(Constants.CURRENT_CONTEXT, "explosion_sound close", Toast.LENGTH_SHORT).show();
    }*/

}
