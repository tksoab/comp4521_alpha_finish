package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.ArrayList;

/**
 * Created by TKs on 19/5/2018.
 */

public class Animation {
    private ArrayList<Bitmap> animation;
    private Bitmap anime;
    private int frameindex;

    private int count;

    public Animation(Bitmap src,int frame,int row, int col){ //explosion 65*65
        animation=new ArrayList<Bitmap>();
        int maxframe=frame;
        frameindex=0;
        count=0;
        int width=src.getWidth()/col;
        int height=src.getHeight()/row;
        /*anime=src;
        System.out.println(anime.getWidth()+" "+anime.getHeight());
        anime=Bitmap.createBitmap(anime,336,672,168,168);
        */
        for(int i=0;i<row;i++) {
            for (int j = 0; j < col; j++) {
                //System.out.println("colxrow "+j+" "+i+" "+col*width+" "+row*height);
                Bitmap temp=Bitmap.createBitmap(src, width*j,height*i,width,height);

                animation.add(temp);
                maxframe--;
                if(maxframe==0){break;}
            }
        }
    }

    public void draw(Canvas canvas, float x, float y){
        count++;
        if(count%5==0){
            frameindex++;
            //frameindex=frameindex>=animation.size()?0:frameindex;
        }
        if(frameindex<animation.size())
        canvas.drawBitmap(animation.get(frameindex),x,y,null);
        //canvas.drawBitmap(anime,x,y,null);
    }

    public void drawForEnemy(Canvas canvas, float x, float y){

            frameindex+=3;
            //frameindex=frameindex>=animation.size()?0:frameindex;
        if(frameindex<animation.size())
        canvas.drawBitmap(animation.get(frameindex),x,y,null);
        //canvas.drawBitmap(anime,x,y,null);
    }

    public boolean checkFinished(){
        if(frameindex>=animation.size()-1)
            return true;
        else return false;
    }
}
