package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

/**
 * Created by chunw on 6/5/2018.
 */

public class BGM extends Service {

    private static final String TAG = null;
    private MediaPlayer player;
    private final IBinder mBinder = new BGM.LocalBinder();
    private boolean status;

    public IBinder onBind(Intent intent){
        return mBinder;
    }

    public class LocalBinder extends Binder {
        BGM getService() {
            // Return this instance of hk.ust.cse.comp4521.comp4521_pj_sample.LocalService so clients can call public methods
            return BGM.this;
        }
    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //player = MediaPlayer.create(getApplicationContext(), R.raw.jingle);
        //player.setLooping(true);
        //player.start();
        return START_STICKY;

    }

    public void play(){
        player = MediaPlayer.create(getApplicationContext(), R.raw.maintheme);
        player.start();
    }

    public void stop(){
        if(player!=null) {
            player.stop();
            //player.release();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(player!=null) {
            player.stop();
            player.release();
        }
    }


}
