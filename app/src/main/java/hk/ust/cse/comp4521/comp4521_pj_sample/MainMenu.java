package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.os.IBinder;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


public class MainMenu extends Activity {
    public static final String EXTRA_MESSAGE = "hk.ust.cse.comp4521.comp4521_pj_sample";
    //public static MediaPlayer mediaPlayer;

    public static String file_name = "Option.txt";
    public static String file_content = "1/n1";
    String result = file_content;

    public static BGM mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.jingle);


        if(getFilesDir().list().length>0){
            result = FileReadWrite.readFile(this, file_name);
        }
        else if(getFilesDir().list().length==0) {
            FileReadWrite.writeFile(this,file_name,file_content);
        }

        setContentView(R.layout.activity_main);

        Intent service = new Intent(this, BGM.class);
        //   Toast.makeText(getApplicationContext(), "intent", Toast.LENGTH_SHORT).show();
        bindService(service, mServiceConnection, Context.BIND_AUTO_CREATE);
        //   Toast.makeText(getApplicationContext(), "bind", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onResume(){
        FileReadWrite.readFile(this, file_name);
        Toast.makeText(getApplicationContext(),"OnResume", Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    protected  void onPause(){
        Toast.makeText(getApplicationContext(),"OnPause", Toast.LENGTH_SHORT).show();
        super.onPause();
    }

    @Override
    protected void onStop(){
        //stopService(new Intent(this, BGM.class));
        //mService.stop();
        Toast.makeText(getApplicationContext(),"OnStop", Toast.LENGTH_SHORT).show();
        // unbindService(mServiceConnection);
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        //stopService(new Intent(this, BGM.class));
        // mService.stop();
        Toast.makeText(getApplicationContext(),"OnDestroy", Toast.LENGTH_SHORT).show();
        unbindService(mServiceConnection);
        super.onDestroy();
    }


    public void play(View view){
        Intent intent = new Intent(this, MainGame.class);
        startActivity(intent);
    }

    public void option(View view){
        Intent intent = new Intent(this, OptionMenu.class);
        startActivity(intent);
    }
    public void score(View view){
        Intent intent = new Intent(this, ScoreMenu.class);
        startActivity(intent);
    }
    /*
        public void readFile(String FileName){
            FileInputStream file_op;
            try {
                Toast.makeText(getApplicationContext(), "file found", Toast.LENGTH_SHORT).show();
                StringBuilder sb = new StringBuilder();
                file_op = this.openFileInput(FileName);
                byte[] data = new byte[file_op.available()];
                while(file_op.read(data)!=-1){
                    sb.append(new String(data));
                }
                file_op.close();
                result = sb.toString();
              //  Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
            }catch(FileNotFoundException e){
                e.printStackTrace();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        public void writeFile(String FileName, String FileContent){
            FileOutputStream file_op;
            try {
                file_op = openFileOutput(FileName, Context.MODE_PRIVATE);
                file_op.write(FileContent.getBytes());
                file_op.close();
           //   Toast.makeText(getApplicationContext(), "Option.txt create", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
           //     Toast.makeText(getApplicationContext(), "no create", Toast.LENGTH_SHORT).show();
            }
        }
    */
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BGM.LocalBinder serviceBinder = (BGM.LocalBinder) service;
            mService = serviceBinder.getService();
            Toast.makeText(getApplicationContext(), "mService = serviceBinder.getService();", Toast.LENGTH_SHORT).show();
            if(result.endsWith("1")){
                mService.play();
                //        Toast.makeText(getApplicationContext(), "play", Toast.LENGTH_SHORT).show();
            }
            else if(result.endsWith("0")){

                mService.stop();
                //        Toast.makeText(getApplicationContext(), "stop", Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };
}
