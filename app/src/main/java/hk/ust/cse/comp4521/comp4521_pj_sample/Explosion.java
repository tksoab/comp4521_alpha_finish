package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.BitmapFactory;

/**
 * Created by TKs on 19/5/2018.
 */

public class Explosion extends Animation {
    public Explosion(){
        super(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.explosion),23,5,5);
    }
}
