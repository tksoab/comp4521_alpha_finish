package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by chunw on 19/5/2018.
 */

public class FileReadWrite extends AppCompatActivity{

    public static String readFile(Context c, String FileName){
        FileInputStream file_op;
        String result = null;
        try {
           // Toast.makeText(getApplicationContext(), "file found", Toast.LENGTH_SHORT).show();
            StringBuilder sb = new StringBuilder();
            file_op = c.openFileInput(FileName);
            byte[] data = new byte[file_op.available()];
            while(file_op.read(data)!=-1){
                sb.append(new String(data));
            }
            file_op.close();
            result = sb.toString();
            //  Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public static void writeFile(Context c, String FileName, String FileContent){
        FileOutputStream file_op;
        try {
            file_op = c.openFileOutput(FileName, Context.MODE_PRIVATE);
            file_op.write(FileContent.getBytes());
            file_op.close();
            //   Toast.makeText(getApplicationContext(), "Option.txt create", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            //     Toast.makeText(getApplicationContext(), "no create", Toast.LENGTH_SHORT).show();
        }
    }
}
