package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Canvas;

/**
 * Created by TKs on 19/5/2018.
 */

public interface GameObjects {

    //public void update(int x,int y);
    public void draw(Canvas canvas);
}
