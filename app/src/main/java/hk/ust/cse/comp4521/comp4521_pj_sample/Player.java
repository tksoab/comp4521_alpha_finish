package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.MediaPlayer;

/**
 * Created by TKs on 19/5/2018.
 */

public class Player implements GameObjects{
    private int x,y;
    private int health;
    private Bitmap objImage;
    private Bitmap bitmapbase;
    private Bitmap bitmaptop;
    private int degree;
    private int width;
    private int height;
    private Explosion explosion;

    private Bitmap newtop;
    private Bitmap newbase;

    private boolean alive;
    private int totalHealth;

    private int left;
    private int right;
    private int top;
    private int bottom;

    public Player(int x, int y,int hp){
        super();
        this.x=x;
        this.y=y;

        bitmapbase= BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.playertankbase);
        bitmaptop=BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.playertanktop);
        newtop=bitmaptop;
        newbase=bitmapbase;
        objImage=HelperFunction.overlayBitmapToCenter(bitmapbase,bitmaptop);
        width=objImage.getWidth();
        height=objImage.getHeight();
        degree=0;
        alive=true;
        explosion=new Explosion();

        totalHealth=hp;
        health=hp;

        left=this.x-objImage.getWidth()/2;
        right=this.x+objImage.getWidth()/2;
        top=this.y-objImage.getHeight()/2;
        bottom=this.y+objImage.getHeight()/2;
    }

    public void update(int x,int y){
        this.x=x;
        this.y=y;
        left=this.x-objImage.getWidth()/2;
        right=this.x+objImage.getWidth()/2;
        top=this.y-objImage.getHeight()/2;
        bottom=this.y+objImage.getHeight()/2;
        //System.out.println("player at "+x+" "+y);
    }

    public void updateDirection(int degree,int pos){//pos =0  base ,,, pos=1 top
        this.degree=degree;
        if(pos==1){
        newtop=HelperFunction.RotateBitmap(bitmaptop,this.degree);
        newbase=newbase;
        }else if(pos==0){
            newbase=HelperFunction.RotateBitmap(bitmapbase,this.degree);
            newtop=newtop;
        }else{
            newtop=newtop;
            newbase=newbase;
        }
        this.objImage=HelperFunction.overlayBitmapToCenter(newbase,newtop);
    }

    public void draw(Canvas canvas){
        if(health>0)
            canvas.drawBitmap(objImage,(Constants.SCREEN_WIDTH-objImage.getWidth())/2,(Constants.SCREEN_HEIGHT-objImage.getHeight())/2,null);
        else if(alive==true){ explosion.draw(canvas,(Constants.SCREEN_WIDTH-objImage.getWidth())/2,(Constants.SCREEN_HEIGHT-objImage.getHeight())/2);
        if(explosion.checkFinished()) {alive=false;}
        }
    }

    public Bullet fire(){
        Bullet bullet=new Bullet(x,y,degree, BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.bulletpixel));

        return bullet;
    }

    public void setHealth(int hp){
        health+=hp;
    }
    public int returnHealth(){
        return health;
    }

    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public void playExplosionSound(){
        if(MainGame.sound_set.equals("1"))
            Constants.EXPLOSION_SOUND.start();
    }

    public boolean EnemyInDirection(){

        return false;
    }
    public int returnTotalHealth(){
        return totalHealth;
    }
}

