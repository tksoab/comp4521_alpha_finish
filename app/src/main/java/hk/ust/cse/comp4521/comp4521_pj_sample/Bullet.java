package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by TKs on 19/5/2018.
 */

public class Bullet {
    private int x,y;
    private int speed;
    private int index;
    private int dx,dy;
    private Bitmap image;
    private int[] hit;

    private int timeToLive;

    private int left;
    private int right;
    private int top;
    private int bottom;

    public Bullet(int x,int y,int degree,Bitmap res){
        this.x=x;
        this.y=y;
        this.speed=90;
        this.index=degree/5;
        dx=Constants.TABLEOFDXDY[this.index][0];
        dy=Constants.TABLEOFDXDY[this.index][1];

        image=res;
        left=this.x-image.getWidth()/2;
        right=this.x+image.getWidth()/2;
        top=this.y-image.getHeight()/2;
        bottom=this.y+image.getHeight()/2;
        timeToLive=500;


    }
    public void update(){
        x+=dx;
        y+=dy;
        timeToLive--;
        left=x-image.getWidth()/2;
        right=x+image.getWidth()/2;
        top=y-image.getHeight()/2;
        bottom=y+image.getHeight()/2;

        //System.out.println("Bullet is moving to "+this.x+" "+this.y);
    }
    public void draw(Canvas canvas){
        if(HelperFunction.checkWithinCanvas(this.x,this.y))
        {
            canvas.drawBitmap(image,x-Constants.Left-image.getWidth()/2,y-Constants.Top-image.getHeight()/2,null);
        }
    }

    public boolean checkWithinMap(){
        if(x<0||x>Constants.MAP_WIDTH||y<0||y>Constants.MAP_HEIGHT)
            return false;
        else return true;
    }

    public boolean checkEnemyCollision(int x, int y){
        //System.out.println(this.x+" "+this.y+" "+x+" "+y);
        if(this.x>=x-60&&this.x<=x+60&&this.y>=y-60&&this.y<=y+60) {
            hit=new int[]{x,y};
            return true;
        }
        else return false;
    }

    public int returntimeToLive(){return timeToLive;}
    public int[] returnHitXY(){
        return hit;
    }
}
