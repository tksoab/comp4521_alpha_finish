package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by TKs on 19/5/2018.
 */

class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private Map map;
    private static boolean gamestart=false;

    public GamePanel(Context context) {
        super(context);

        getHolder().addCallback(this);

        Constants.CURRENT_CONTEXT=context;
        //Constants.EXPLOSION=new Explosion();;
        gamestart=false;
        //thread = new MainThread(getHolder(),this);

        Constants.PLAYER_SHOOT_SOUND = MediaPlayer.create(context,R.raw.shoot2);
        Constants.EXPLOSION_SOUND=MediaPlayer.create(context,R.raw.explosion);
        Constants.ENEMY_GET_HIT_SOUND =MediaPlayer.create(context,R.raw.enemyhitbybullet);
        Constants.PLAYER_GET_HIT_SOUND=MediaPlayer.create(context,R.raw.playerhitbybullet);
        Constants.ENEMY_SHOOT_SOUND=MediaPlayer.create(context,R.raw.enemyshoot);
        Constants.HEALTHBARIMAGE=new HealthBarImage();

        map=new Map(context,BitmapFactory.decodeResource(context.getResources(),R.drawable.newmapv2));
        //map.setVector(-5);
        //map.setMoveSpeed(50);
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}

    @Override
    public void surfaceCreated(SurfaceHolder holder){
        thread = new MainThread(getHolder(), this);

        Constants.INIT_TIME = System.currentTimeMillis();

        thread.setRunning(true);
        thread.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){
        boolean retry = true;
        while(retry){
            try {
                thread.setRunning(false);
                thread.join();
            }catch(Exception e){e.printStackTrace();}
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){



        super.onTouchEvent(event);
        if(gamestart==false)
            if(event.getAction()==MotionEvent.ACTION_DOWN){gamestart=true;}

        if(gamestart==true)
            map.receiveTouch(event);

        if(map.gameover==true){
            if(event.getAction()==MotionEvent.ACTION_DOWN){
                resetGame();
            }
        }
        return true;
        //return super.onTouchEvent(event);
    }

    public void updateMap(double pitch,double roll) {
        if (gamestart == true && map.gameover == false) {
            map.updateMap(pitch, roll);
        }
    }
    public void update(){
        if(gamestart==true&& map.gameover == false)map.update();

    }
/*
    public void stopSound(){
        map.shoot_sound.release();
        map.shoot_sound = null;
        Toast.makeText(Constants.CURRENT_CONTEXT, "shoot_sound close", Toast.LENGTH_SHORT).show();
        map.stopexplosion_sound();
    }*/

    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);
        if(map!=null)
        map.draw(canvas);
        if(gamestart==false){
            Paint paint=new Paint();
            paint.setColor(Color.BLUE);
            paint.setTextSize(100);
            canvas.drawText("Click to Start",Constants.SCREEN_WIDTH/3,Constants.SCREEN_HEIGHT/3,paint);
        }

    }
    public void resetGame(){
        this.map=new Map(Constants.CURRENT_CONTEXT,BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.newmapv2));
        map.gameover=false;
        gamestart=false;
    }

}
