package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.ArrayList;

public class ScoreImage {
    private String score;
    private Bitmap scoreText;
    private int scoreLength;
    private int totalWidth;
    private int totalHeight;
    private ArrayList<Bitmap> sequence;
    private int widthPerNum;
    private Bitmap scoreboard;
    public ScoreImage(int score){
        this.score=score+"";
        scoreText=BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.score);
        Bitmap numSequence=BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.number77);
        totalWidth=scoreText.getWidth();
        totalHeight=scoreText.getHeight();
        widthPerNum=numSequence.getWidth()/10;
        scoreLength= this.score.length();
        sequence=new ArrayList<Bitmap>();
        for(int i=0;i<scoreLength;i++){
            int digit=this.score.charAt(i)-48;
            sequence.add(Bitmap.createBitmap(numSequence,digit*widthPerNum,0,widthPerNum,totalHeight));
            totalWidth+=widthPerNum;
        }
        scoreboard=Bitmap.createBitmap(totalWidth,totalHeight, Bitmap.Config.ARGB_8888);
        Canvas mergerImage=new Canvas(scoreboard);

        mergerImage.drawBitmap(scoreText,0,0,null);

        for(Bitmap b:sequence){
            int index=sequence.indexOf(b);
            mergerImage.drawBitmap(b,scoreText.getWidth()+widthPerNum*index,0,null);
        }
    }

    public Bitmap returnScoreBoard(){


        return scoreboard;
    }

    private int returnPowerOfTen(int digit){
        int result=1;
        for(int i=0;i<digit-1;i++){
            result*=10;
        }
        return result;
    }
}
