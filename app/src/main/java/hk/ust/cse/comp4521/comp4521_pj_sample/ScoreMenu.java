package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ScoreMenu extends AppCompatActivity {

    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
    }

    @Override
    protected void onStart(){
        super.onStart();
        TextView score = findViewById(R.id.score);
        result = FileReadWrite.readFile(this, "Score.txt");
        String score_s = result;
        score.setText(score_s);
    }


}
