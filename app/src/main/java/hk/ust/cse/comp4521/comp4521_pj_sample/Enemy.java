package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

/**
 * Created by TKs on 19/5/2018.
 */


public class Enemy implements GameObjects {
    private int x,y;
    private int health;
    private static Bitmap objImage=null;
    private Bitmap editedObjImage;

    private Bitmap bitmapbase;
    private Bitmap bitmaptop;

    private Bitmap partial;
    private int partx,party,partwidth,partheight;

    private int index;//to indicate the direction (by angle/5)

    private boolean alive;

    private int left;
    private int right;
    private int top;
    private int bottom;
    private Explosion explosion;
    private int fireCooldown=0;


    public Enemy(int x, int y){
        this.x=x;
        this.y=y;
        if(objImage==null) {
            Bitmap imagebase = BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.enemytankbase);
            Bitmap imagetop = BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.enemytanktop);
            objImage = HelperFunction.overlayBitmapToCenter(imagebase, imagetop);
        }
        partial=Bitmap.createBitmap(objImage);

        health=5;
        alive=true;

        explosion=new Explosion();
        partheight=partial.getHeight();
        partwidth=partial.getWidth();

        left=this.x-objImage.getWidth()/2;
        right=this.x+objImage.getWidth()/2;
        top=this.y-objImage.getHeight()/2;
        bottom=this.y+objImage.getHeight()/2;

        index=0;
    }

    public void update(int x,int y){

/*to be modify
        float angle = (float) Math.toDegrees(Math.atan2(this.x-x,this.y-y))+90;
        if(angle < 0){
            angle += 360;
        }*/
/**/

        if(fireCooldown>0)
            fireCooldown--;

        index=updateDirection(x,y);
        int dx=Constants.TABLEOFDXDY[this.index][0]/4;
        int dy=Constants.TABLEOFDXDY[this.index][1]/4;
        editedObjImage=HelperFunction.RotateBitmap(objImage,index*5);
        if(!checkIfNearPlayer(x,y)) {
            this.x += dx;
            this.y += dy;
        }

        left=this.x-objImage.getWidth()/2;
        right=this.x+objImage.getWidth()/2;
        top=this.y-objImage.getHeight()/2;
        bottom=this.y+objImage.getHeight()/2;
        updatePartial();

    }

    public void draw(Canvas canvas){
        if(checkWithinCanvasArea()) {
            float canvasleft;
            float canvastop;
            if(left<Constants.Left){
                canvasleft=0;
            }else canvasleft=left-Constants.Left;
            if(top<Constants.Top){
                canvastop=0;
            }else canvastop=top-Constants.Top;

            if(health>0)
                canvas.drawBitmap(partial,canvasleft,canvastop,null);
            else if(alive==true){ explosion.drawForEnemy(canvas,canvasleft,canvastop);
                if(explosion.checkFinished()) {alive=false;}
            } }
        //else {Constants.EXPLOSION.draw(canvas,canvasleft,canvastop);}
        //canvas.drawBitmap(partial, (Constants.SCREEN_WIDTH - objImage.getWidth()) / 2, (Constants.SCREEN_HEIGHT - objImage.getHeight()) / 2, null); //to be modify

    }
    public boolean checkWithinCanvasArea(){
        if(left<Constants.Right&&right>Constants.Left && top<Constants.Bottom && bottom>Constants.Top)
            return true;
        else return false;

        /*
        if(this.x>=Constants.CenterX-Constants.SCREEN_WIDTH/2-10&& this.x<=Constants.CenterX+Constants.SCREEN_WIDTH/2+10)
            if(this.y>=Constants.CenterY-Constants.SCREEN_HEIGHT/2-10&& this.y<=Constants.CenterY+Constants.SCREEN_HEIGHT/2+10)
                return true;
            else return false;
        else return false;*/
    }

    private void updatePartial(){
        if(checkWithinCanvasArea()){
            if(top<Constants.Top){
                partheight=bottom-Constants.Top;
                party=Constants.Top-top;
            }else if(bottom>Constants.Bottom){
                partheight=Constants.Bottom-top;
                party=0;
            }else {
                partheight = objImage.getHeight();
                party=0;
            }
            if(left<Constants.Left){
                partwidth=right-Constants.Left;
                partx=Constants.Left-left;
            }else if(right>Constants.Right){
                partwidth=Constants.Right-left;
                partx=0;
            }else {
                partwidth = objImage.getWidth();
                partx=0;
            }
            //System.out.println("partx and party is "+partx+" "+party);
            partial=Bitmap.createBitmap(editedObjImage,partx,party,partwidth,partheight);
            /*
            int tempx;
            int tempy;
            int tempwidth;
            int tempheight;
            if(this.x>Constants.CenterX) {
                partx = this.x - Constants.CenterX + Constants.SCREEN_WIDTH / 2 - objImage.getWidth() / 2;
                tempx=0;
                if(this.x+objImage.getWidth()/2-Constants.CenterX-Constants.SCREEN_WIDTH/2<0){
                    partwidth=objImage.getWidth();
                }else{
                    partwidth=this.x+objImage.getWidth()/2-Constants.CenterX-Constants.SCREEN_WIDTH/2;
                }
            }
            else {
                partx = Constants.CenterX - this.x;
            }*/
            //partial=Bitmap.createBitmap(objImage,)
        }
    }

    public int updateDirection(int x,int y){
        int dy=this.x-x;
        int dx=this.y-y;

        float angle = (float) Math.toDegrees(Math.atan2(Math.abs(dy),Math.abs(dx)));
        if(dy<=0&&dx<=0){
            angle=180-angle;
        }else if(dy<=0&&dx>0){
            angle=angle;
        }else if(dy>0&&dx<=0){
            angle=180+angle;
        }else if(dy>0&&dy>0){
            angle=360-angle;
        }else angle=angle;
        //System.out.println(this.x+" "+this.y+" "+x+" "+y+" "+dy+" "+dx+" "+angle);
        return ((int)angle)/5;
    }

    public boolean checkIfNearPlayer(int x,int y){
        if(Math.abs(this.x-x)<300&&Math.abs(this.y-y)<300)
            return true;

        return false;
    }

    public void setHealth(int hp){
        health+=hp;
    }
    public int returnHealth(){
        return health;
    }


    public boolean checkNearbyFriend(int x, int y){
        //System.out.println(this.x+" "+this.y+" "+x+" "+y);
        if(Math.abs(this.x-x)<=300&&Math.abs(this.y-y)<=300) {
            return true;
        }
        else return false;
    }
    public void updateNotMoving(int x,int y){


        if(fireCooldown>0)
            fireCooldown--;

        index=updateDirection(x,y);
        editedObjImage=HelperFunction.RotateBitmap(objImage,index*5);

        left=this.x-objImage.getWidth()/2;
        right=this.x+objImage.getWidth()/2;
        top=this.y-objImage.getHeight()/2;
        bottom=this.y+objImage.getHeight()/2;
        updatePartial();

    }

    public Bullet fire(){
        Bullet bullet=new Bullet(x,y,index*5, BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.enemybulletpixel));

        return bullet;
    }

    public void setFireCooldown(int cd){
        fireCooldown+=cd;
    }

    public int returnFireCooldown(){
        return fireCooldown;
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public boolean isAlive(){return alive;}
    public void playExplosionSound(){
        if(MainGame.sound_set.equals("1"))
            Constants.EXPLOSION_SOUND.start();
    }

}
