package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class MainGame extends Activity implements SensorEventListener {

    private String result;
    public static String sound_set;
    private SensorManager mSensorManager;
    private GamePanel gamepanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        result = FileReadWrite.readFile(getApplicationContext(), "Option.txt");
        String [] tmp = result.split("/n");
        sound_set = tmp[0];

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH=dm.widthPixels;
        Constants.SCREEN_HEIGHT=dm.heightPixels;
        gamepanel=new GamePanel(this);
        setContentView(gamepanel);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_GAME);
    }

    float[] mags = new float[3];
    float[] accels = new float[3];
    float[] RotationMat = new float[9];
    float[] InclinationMat = new float[9];
    float[] attitude = new float[3];
    final static double RAD2DEG = 180/Math.PI;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        int type = sensorEvent.sensor.getType();

        switch(type) {
            case Sensor.TYPE_ACCELEROMETER:
                accels = sensorEvent.values;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mags = sensorEvent.values;

                break;
        }

        SensorManager.getRotationMatrix(RotationMat,
                InclinationMat, accels, mags);
        SensorManager.getOrientation(RotationMat, attitude);

        double yaw = attitude[0]*RAD2DEG;
        double pitch = attitude[1]*RAD2DEG;
        double roll = attitude[2]*RAD2DEG;

        gamepanel.updateMap(pitch,roll);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
/*
    @Override
    protected void onDestroy(){
        super.onDestroy();
        gamepanel.stopSound();
    }*/
}
