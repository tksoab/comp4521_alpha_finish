package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;

public class HealthBarImage {
    private int frame;
    private ArrayList<Bitmap> barSequence;
    public HealthBarImage(){
        barSequence=new ArrayList<Bitmap>();
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health0));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health1));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health2));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health3));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health4));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health5));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health6));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health7));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health8));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health9));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health10));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health11));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health12));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health13));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health14));
        barSequence.add(BitmapFactory.decodeResource(Constants.CURRENT_CONTEXT.getResources(),R.drawable.health15));
    }
    public Bitmap returnHealthBarImage(int hp,int fullhp){
        if(hp<=0){return barSequence.get(0);}
        else{
            int index=Math.round(((hp*1.0f)/fullhp)*15);
            if(index==0)index=1;
            return barSequence.get(index);
        }
    }
}
