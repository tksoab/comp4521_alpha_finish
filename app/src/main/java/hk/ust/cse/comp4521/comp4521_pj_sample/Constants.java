package hk.ust.cse.comp4521.comp4521_pj_sample;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by TKs on 19/5/2018.
 */

public class Constants {
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;

    public static Context CURRENT_CONTEXT;

    public static int CenterX;
    public static int CenterY;
    public static int Left;
    public static int Right;
    public static int Top;
    public static int Bottom;

    public static int MAP_HEIGHT;
    public static int MAP_WIDTH;

    public static long INIT_TIME;
    public static MediaPlayer EXPLOSION_SOUND;
    public static MediaPlayer PLAYER_SHOOT_SOUND;
    public static MediaPlayer ENEMY_GET_HIT_SOUND;
    public static MediaPlayer PLAYER_GET_HIT_SOUND;
    public static MediaPlayer ENEMY_SHOOT_SOUND;
    //public static Explosion EXPLOSION;

    public static HealthBarImage HEALTHBARIMAGE;

    //public static int diffculty;

    //degree start from top(0) to right(90) to bottom(180) to left(270), increment by 5
    public static final int[][] TABLEOFDXDY={
            {0,-90},{5,-85},{10,-80},{15,-75},{20,-70},{25,-65},{30,-60},{35,-55},{40,-50}, //0-40
            {45,-45},{50,-40},{55,-35},{60,-30},{65,-25},{70,-20},{75,-15},{80,-10},{85,-5},
            {90,0},{85,5},{80,10},{75,15},{70,20},{65,25},{60,30},{55,35},{50,40},
            {45,45},{40,50},{35,55},{30,60},{25,65},{20,70},{15,75},{10,80},{5,85},
            {0,90},{-5,85},{-10,80},{-15,75},{-20,70},{-25,65},{-30,60},{-35,55},{-40,50},
            {-45,45},{-50,40},{-55,35},{-60,30},{-65,25},{-70,20},{-75,15},{-80,10},{-85,5},
            {-90,0},{-85,-5},{-80,-10},{-75,-15},{-70,-20},{-65,-25},{-60,-30},{-55,-35},{-50,-40},
            {-45,-45},{-40,-50},{-35,-55},{-30,-60},{-25,-65},{-20,-70},{-15,-75},{-10,-80},{-5,-85}};
}
